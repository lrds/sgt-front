import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { PoModule } from '@po-ui/ng-components';
import { AppRoutingModule } from './app-routing.module';
import { ListaTarefaComponent } from './tarefa/lista/lista.component';
import { FormularioTarefaComponent } from './tarefa/formulario/formulario.component';
import { ListaUsuarioComponent } from './usuario/lista/lista.component';
import { FormularioUsuarioComponent } from './usuario/formulario/formulario.component';
import { ListaCursoComponent } from './curso/lista/lista.component';
import { FormularioCursoComponent } from './curso/formulario/formulario.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaTarefaComponent,
    FormularioTarefaComponent,
    ListaUsuarioComponent,
    FormularioUsuarioComponent,
    ListaCursoComponent,
    FormularioCursoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PoModule,
    AppRoutingModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
