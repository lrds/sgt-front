import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Status } from './status.model';
import { PoNotificationService } from '@po-ui/ng-components';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  urlBase: String = 'http://localhost:4445/status-tarefas';

  constructor(private http: HttpClient,
              private poNotification: PoNotificationService) {}

  buscaTudo(parametros: {pesquisa: string}): Observable<{ hasNext: boolean, items: Status[]}> {
    const url = `${this.urlBase}?pesquisa=${parametros.pesquisa}`
    return this.http.get<{ hasNext: boolean, items: Status[]}>(url)
  }

  mensagem (msg: string) {
    this.poNotification.success(msg);
  }
}
