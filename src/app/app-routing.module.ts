import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioTarefaComponent } from './tarefa/formulario/formulario.component';
import { ListaTarefaComponent } from './tarefa/lista/lista.component';
import { FormularioUsuarioComponent } from './usuario/formulario/formulario.component';
import { ListaUsuarioComponent } from './usuario/lista/lista.component';
import { FormularioCursoComponent } from './curso/formulario/formulario.component';
import { ListaCursoComponent } from './curso/lista/lista.component';

const routes: Routes = [
  { path: 'tarefa', component: ListaTarefaComponent },
  { path: 'tarefa/nova', component: FormularioTarefaComponent },
  { path: 'tarefa/editar/:id', component: FormularioTarefaComponent },
  { path: 'usuario', component: ListaUsuarioComponent },
  { path: 'usuario/novo', component: FormularioUsuarioComponent },
  { path: 'usuario/editar/:id', component: FormularioUsuarioComponent },
  { path: 'curso', component: ListaCursoComponent },
  { path: 'curso/novo', component: FormularioCursoComponent },
  { path: 'curso/editar/:id', component: FormularioCursoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
