import { Component, OnInit, ViewChild } from '@angular/core';
import { PoModalAction, 
         PoModalComponent, 
         PoNotificationService, 
         PoPageAction, 
         PoPageFilter, 
         PoSelectOption, 
         PoTableAction, 
         PoTableColumn } from '@po-ui/ng-components';
import { Tarefa } from '../tarefa.model';
import { TarefaService } from '../tarefa.service';
import { Router } from '@angular/router';
import { StatusService } from '../../status/status.service'

@Component({
  selector: 'app-lista-tarefas',
  templateUrl: './lista.component.html'
})
export class ListaTarefaComponent implements OnInit  {

  private readonly url: String = 'http://localhost:4445/tarefas';

  private parametros: {pagina: Number, 
                       pesquisa: String, 
                       status: Number} = {
    pagina: 0,
    pesquisa: '',
    status: 0
  };

  readonly actions: Array<PoPageAction> = [
    { action: this.criarTarefa.bind(this), label: 'Criar Tarefa' }
  ];

  public readonly filtro: PoPageFilter = {
    action: this.buscaFiltroPaginado.bind(this),
    advancedAction: this.abrirModalFiltro.bind(this),
    placeholder: 'Filtro'
  };

  public stateOptions: Array<PoSelectOption> = [];

  public readonly filtrarStatus: PoModalAction = {
    action: () => {
      this.parametros.pagina = 0;
      this.parametros.status = this.filtroStatus; 
      this.modalFiltro?.close();
      this.buscaTudoPaginado();
    },
    label: 'Filtrar'
  };

  @ViewChild('modalFiltro', { static: true }) modalFiltro: PoModalComponent | undefined;
  @ViewChild('modalUsuarios', { static: true }) modalUsuarios: PoModalComponent | undefined;

  readonly columns: Array<PoTableColumn> = [
    { property: 'codigo', width: '8%', label: 'Código' },
    { property: 'titulo', width: '18%', label: 'Título' },
    { property: 'descricao', width: '25%', label: 'Descrição' },
    { property: 'descStatusTarefa', width: '12%', label: 'Status' },
    { property: 'dataCriacao', type: 'date', format: 'dd/MM/yyyy', width: '13%', label: 'Data de Criação' },
    { property: 'dataFinalizacao', type: 'date', format: 'dd/MM/yyyy', width: '13%', label: 'Data de Finalização' } 
  ];

  public readonly tableActions: Array<PoTableAction> = [
    { action: this.abrirModalUsuarios.bind(this), label: 'Responsáveis' },
    { action: this.editarTarefa.bind(this), label: 'Editar' },
    { action: this.deletarTarefa.bind(this), label: 'Remover', type: 'danger', separator: true }
  ];

  public filtroStatus: Number = 0;
  public tarefas: Array<Tarefa> = [];
  public hasNext: boolean = false;
  public carregando: boolean = false;
  public nomeUsuarios: string = '';

  constructor(private tarefaService: TarefaService, 
              private rotas: Router,
              private poNotification: PoNotificationService,
              private statusService: StatusService) { }

  ngOnInit() {
    this.buscaTudoPaginado();
    this.buscarStatusTarefa();
  }

  private criarTarefa() {
    this.rotas.navigateByUrl('/tarefa/nova');
  }

  private editarTarefa(tarefa: Tarefa) {
    this.rotas.navigateByUrl(`/tarefa/editar/${tarefa.codigo}`);
  }

  private deletarTarefa(tarefa: Tarefa) {
    if (tarefa.codigo) {
      this.carregando = true;

      this.tarefaService.deletar(tarefa.codigo).subscribe(() => {
        this.buscaTudoPaginado();
        this.carregando = false;
        this.mensagem('Tarefa deletada com sucesso!');                                                                     
      }) 
    } 
  }

  private buscaFiltroPaginado(filtro: String) {
    this.parametros.pagina = 0;
    this.parametros.pesquisa = filtro; 
    this.buscaTudoPaginado();
  }  

  private buscaTudoPaginado() {
    this.carregando = true;

    this.tarefaService.buscaTudoPaginado(this.parametros).subscribe((resposta) => {
      console.log(resposta);
      this.tarefas = !this.parametros.pagina || this.parametros.pagina === 1 ? resposta.items
                                                                             : [...this.tarefas, ...resposta.items];
      this.tarefas = this.tarefas.map(tarefas => { 
        return {
          ...tarefas, 
          usuario: 'Responsáveis' 
        }
      });
      this.carregando = false;                                                                       
    }) 
  } 

  private abrirModalFiltro() {
    this.modalFiltro?.open();
  }

  private abrirModalUsuarios(tarefa: Tarefa) {
    console.log('usuarios: ', tarefa);
    this.nomeUsuarios = tarefa.usuarioTarefas?.map(item => item.usuario.nome ).join(', ') || '';
    this.modalUsuarios?.open();
  }

  private buscarStatusTarefa() {
    this.statusService.buscaTudo({pesquisa: ''}).subscribe((resposta) => {
      resposta.items = [{ descricao: 'Tudo', codigo: 0 }, ...resposta.items];
      this.stateOptions = resposta.items.map(status => ({
        label: status.descricao,
        value: status.codigo
      } as unknown as PoSelectOption));
    })
  }

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
