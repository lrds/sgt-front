import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tarefa } from './tarefa.model';

@Injectable({
  providedIn: 'root'
})
export class TarefaService {

  urlBase: String = 'http://localhost:4445/tarefas';

  constructor(private http: HttpClient) {}

  buscaTudoPaginado(parametros: {pagina: Number, 
                                 pesquisa: String,
                                 status: Number}): Observable<{ hasNext: boolean, items: Tarefa[]}> {
    const url = `${this.urlBase}/paginado/lista?page=${parametros.pagina}`+
                                              `&pesquisa=${parametros.pesquisa}`+
                                              `&status=${parametros.status}`
    return this.http.get<{ hasNext: boolean, items: Tarefa[]}>(url)
  }

  criar(tarefa: Tarefa) {
    const url = `${this.urlBase}`
    return this.http.post<Tarefa>(url, tarefa);
  }

  editar(tarefa: Tarefa) {
    const url = `${this.urlBase}`
    return this.http.put<Tarefa>(url, tarefa);
  }

  deletar(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.delete<void>(url)
  }

  buscarPorCodigo(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.get<Tarefa>(url)
  }

}
