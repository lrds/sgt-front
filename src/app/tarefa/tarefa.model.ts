import { Status } from "../status/status.model";

export interface Tarefa {
    codigo?: Number | null;
    titulo?: String;
    descricao?: String;
    cdStatusTarefa?: Number | null;
    descStatusTarefa?: String;
    cdUsuarioTarefas?: Array<any>;
    usuarioTarefas?: Array<any>;
    dataCriacao?: Date | null;
    dataFinalizacao?: Date | null;
}