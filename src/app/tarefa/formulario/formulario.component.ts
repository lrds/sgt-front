import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tarefa } from '../tarefa.model';
import { PoMultiselectOption, PoNotificationService, PoSelectOption } from '@po-ui/ng-components';
import { StatusService } from '../../status/status.service'
import { TarefaService } from '../tarefa.service';
import { UsuarioService } from 'src/app/usuario/usuario.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html'
})
export class FormularioTarefaComponent implements OnInit {

  private parametros: {pesquisa: string} = {
    pesquisa: ''
  };

  public tarefa: Tarefa = {};

  public statusLista!: Array<PoSelectOption>;

  public usuariosLista!: Array<PoMultiselectOption>;

  constructor(private statusService: StatusService,
              private tarefaService: TarefaService,
              private usuarioService: UsuarioService,
              private rotas: Router,
              private rotaAtiva: ActivatedRoute,
              private poNotification: PoNotificationService) { }

  ngOnInit() {
    this.buscarStatusTarefa();
    this.buscarUsuarios();
    this.limpaFormulario();
    this.rotaAtiva.params.subscribe(params => {
      if (params['id']) {
        this.carregarFormulario(params['id']);
      }
    });
  }

  cancelar() {
    this.rotas.navigateByUrl('/tarefa');
  }

  salvar() {
    const tarefa = {...this.tarefa};
    tarefa.codigo ? this.editar(tarefa) : this.criar(tarefa);
  }

  private criar(tarefa: Tarefa) {
    this.tarefaService.criar(tarefa).subscribe((resposta) => {
      this.rotas.navigateByUrl('/tarefa');
      this.mensagem('Tarefa criada com sucesso!');
    })
  }

  private editar(tarefa: Tarefa) {
    this.tarefaService.editar(tarefa).subscribe((resposta) => {
      this.rotas.navigateByUrl('/tarefa');
      this.mensagem('Tarefa editada com sucesso!');
    })
  }

  private buscarStatusTarefa() {
    this.statusService.buscaTudo(this.parametros).subscribe((resposta) => {
      this.statusLista = resposta.items.map(status => ({
        label: status.descricao,
        value: status.codigo
      } as unknown as PoSelectOption));
      this.statusLista && this.statusLista[0] ? this.tarefa.cdStatusTarefa = this.statusLista[0].value as Number : null;
    })
  }

  private buscarUsuarios() {
    this.usuarioService.buscaTudo(this.parametros).subscribe((resposta) => {
      this.usuariosLista = resposta.items.map(usuario => ({
        label: usuario.nome,
        value: usuario.codigo
      } as PoSelectOption));  
    })
  }

  private limpaFormulario() {
    this.tarefa = {
      codigo: null,
      titulo: '',
      descricao: '',
      cdStatusTarefa: null,
      dataCriacao: null,
      dataFinalizacao: null
    };
  }

  private carregarFormulario(id: Number) {
    this.tarefaService.buscarPorCodigo(id).subscribe((resposta) => {
      this.tarefa = resposta;
    })
  }

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
