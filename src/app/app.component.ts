import { Component } from '@angular/core';

import { PoMenuItem } from '@po-ui/ng-components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  readonly menus: Array<PoMenuItem> = [
    { label: 'Cadastro de Tarefas', link: '/tarefa', icon: 'po-icon-calendar-ok', shortLabel: 'Tarefas'},
    { label: 'Cadastro de Usuários', link: '/usuario', icon: 'po-icon-users', shortLabel: 'Usuários'},
    { label: 'Cadastro de Cursos', link: '/curso', icon: 'po-icon-warehouse', shortLabel: 'Cursos'}
  ];

}
