import { Component, OnInit } from '@angular/core';
import { PoNotificationService, 
         PoPageAction, 
         PoPageFilter, 
         PoTableAction, 
         PoTableColumn } from '@po-ui/ng-components';
import { Curso } from '../curso.model';
import { CursoService } from '../curso.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista.component.html'
})
export class ListaCursoComponent implements OnInit  {

  private readonly url: String = 'http://localhost:4445/cursos';

  private parametros: {pagina: Number, 
                       pesquisa: String} = {
    pagina: 0,
    pesquisa: ''
  };

  readonly actions: Array<PoPageAction> = [
    { action: this.criarCurso.bind(this), label: 'Criar Curso' }
  ];

  public readonly filtro: PoPageFilter = {
    action: this.buscaFiltroPaginado.bind(this),
    placeholder: 'Filtro'
  }

  readonly columns: Array<PoTableColumn> = [
    { property: 'codigo', width: '30%', label: 'Código' },
    { property: 'descricao', width: '70%', label: 'Nome' } 
  ];

  public readonly tableActions: Array<PoTableAction> = [
    { action: this.editarCurso.bind(this), label: 'Editar' },
    { action: this.deletarCurso.bind(this), label: 'Remover', type: 'danger', separator: true }
  ];

  public filtroStatus: Number = 0;
  public cursos: Array<Curso> = [];
  public hasNext: boolean = false;
  public carregando: boolean = false;

  constructor(private cursoService: CursoService, 
              private rotas: Router,
              private poNotification: PoNotificationService) { }

  ngOnInit() {
    this.buscaTudoPaginado();
  }

  private criarCurso() {
    this.rotas.navigateByUrl('/curso/novo');
  }

  private editarCurso(curso: Curso) {
    this.rotas.navigateByUrl(`/curso/editar/${curso.codigo}`);
  }

  private deletarCurso(curso: Curso) {
    if (curso.codigo) {
      this.carregando = true;

      this.cursoService.deletar(curso.codigo).subscribe(() => {
        this.buscaTudoPaginado();
        this.carregando = false;
        this.mensagem('Curso deletado com sucesso!');                                                                     
      }) 
    } 
  }

  private buscaFiltroPaginado(filtro: String) {
    this.parametros.pagina = 0;
    this.parametros.pesquisa = filtro; 
    this.buscaTudoPaginado();
  }  

  private buscaTudoPaginado() {
    this.carregando = true;

    this.cursoService.buscaTudoPaginado(this.parametros).subscribe((resposta) => {
      console.log(resposta);
      this.cursos = !this.parametros.pagina || this.parametros.pagina === 1 ? resposta.items
                                                                              : [...this.cursos, ...resposta.items];
      this.carregando = false;                                                                       
    }) 
  } 

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
