import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Curso } from './curso.model';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  urlBase: String = 'http://localhost:4445/cursos';

  constructor(private http: HttpClient) {}

  buscaTudoPaginado(parametros: {pagina: Number, 
                                 pesquisa: String}): Observable<{ hasNext: boolean, items: Curso[]}> {
    const url = `${this.urlBase}/paginado/lista?page=${parametros.pagina}`+
                                              `&pesquisa=${parametros.pesquisa}`
    return this.http.get<{ hasNext: boolean, items: Curso[]}>(url)
  }

  criar(curso: Curso) {
    const url = `${this.urlBase}`
    return this.http.post<Curso>(url, curso);
  }

  editar(curso: Curso) {
    const url = `${this.urlBase}`
    return this.http.put<Curso>(url, curso);
  }

  deletar(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.delete<void>(url)
  }

  buscarPorCodigo(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.get<Curso>(url)
  }

}
