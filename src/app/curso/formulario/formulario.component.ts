import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Curso } from '../curso.model';
import { PoNotificationService, PoSelectOption } from '@po-ui/ng-components';
import { CursoService } from '../curso.service';

@Component({
  selector: 'app-formulario-curso',
  templateUrl: './formulario.component.html'
})
export class FormularioCursoComponent implements OnInit {

  private parametros: {pesquisa: string} = {
    pesquisa: ''
  };

  public curso: Curso = {};

  public stateOptions: Array<PoSelectOption> = [];

  constructor(private cursoService: CursoService,
              private rotas: Router,
              private rotaAtiva: ActivatedRoute,
              private poNotification: PoNotificationService) { }

  ngOnInit() {
    this.limpaFormulario()
    this.rotaAtiva.params.subscribe(params => {
      if (params['id']) {
        this.carregarFormulario(params['id']);
      }
    });
  }

  cancelar() {
    this.rotas.navigateByUrl('/curso');
  }

  salvar() {
    const curso = {...this.curso};
    curso.codigo ? this.editar(curso) : this.criar(curso);
  }

  private criar(curso: Curso) {
    this.cursoService.criar(curso).subscribe((resposta) => {
      this.rotas.navigateByUrl('/curso');
      this.mensagem('Curso criado com sucesso!');
    })
  }

  private editar(curso: Curso) {
    this.cursoService.editar(curso).subscribe((resposta) => {
      this.rotas.navigateByUrl('/curso');
      this.mensagem('Curso editado com sucesso!');
    })
  }

  private limpaFormulario() {
    this.curso = {
      codigo: null,
      descricao: ''
    };
  }

  private carregarFormulario(id: Number) {
    this.cursoService.buscarPorCodigo(id).subscribe((resposta) => {
      this.curso = resposta;
    })
  }

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
