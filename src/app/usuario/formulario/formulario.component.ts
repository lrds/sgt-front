import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from '../usuario.model';
import { PoNotificationService, PoSelectOption } from '@po-ui/ng-components';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-formulario-usuario',
  templateUrl: './formulario.component.html'
})
export class FormularioUsuarioComponent implements OnInit {

  private parametros: {pesquisa: string} = {
    pesquisa: ''
  };

  public usuario: Usuario = {};

  public stateOptions: Array<PoSelectOption> = [];

  constructor(private usuarioService: UsuarioService,
              private rotas: Router,
              private rotaAtiva: ActivatedRoute,
              private poNotification: PoNotificationService) { }

  ngOnInit() {
    this.limpaFormulario()
    this.rotaAtiva.params.subscribe(params => {
      if (params['id']) {
        this.carregarFormulario(params['id']);
      }
    });
  }

  cancelar() {
    this.rotas.navigateByUrl('/usuario');
  }

  salvar() {
    const usuario = {...this.usuario};
    usuario.codigo ? this.editar(usuario) : this.criar(usuario);
  }

  private criar(usuario: Usuario) {
    this.usuarioService.criar(usuario).subscribe((resposta) => {
      this.rotas.navigateByUrl('/usuario');
      this.mensagem('Usuário criado com sucesso!');
    })
  }

  private editar(usuario: Usuario) {
    this.usuarioService.editar(usuario).subscribe((resposta) => {
      this.rotas.navigateByUrl('/usuario');
      this.mensagem('Usuário editado com sucesso!');
    })
  }

  private limpaFormulario() {
    this.usuario = {
      codigo: null,
      nome: ''
    };
  }

  private carregarFormulario(id: Number) {
    this.usuarioService.buscarPorCodigo(id).subscribe((resposta) => {
      this.usuario = resposta;
    })
  }

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
