import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from './usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  urlBase: String = 'http://localhost:4445/usuarios';

  constructor(private http: HttpClient) {}

  buscaTudo(parametros: {pesquisa: String}): Observable<{ hasNext: boolean, items: Usuario[]}> {
    const url = `${this.urlBase}/paginado/lista?pesquisa=${parametros.pesquisa}`
    return this.http.get<{ hasNext: boolean, items: Usuario[]}>(url)
  }

  buscaTudoPaginado(parametros: {pagina: Number, 
                                 pesquisa: String}): Observable<{ hasNext: boolean, items: Usuario[]}> {
    const url = `${this.urlBase}/paginado/lista?page=${parametros.pagina}`+
                                              `&pesquisa=${parametros.pesquisa}`
    return this.http.get<{ hasNext: boolean, items: Usuario[]}>(url)
  }

  criar(usuario: Usuario) {
    const url = `${this.urlBase}`
    return this.http.post<Usuario>(url, usuario);
  }

  editar(usuario: Usuario) {
    const url = `${this.urlBase}`
    return this.http.put<Usuario>(url, usuario);
  }

  deletar(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.delete<void>(url)
  }

  buscarPorCodigo(id: Number) {
    const url = `${this.urlBase}/${id}`
    return this.http.get<Usuario>(url)
  }

}
