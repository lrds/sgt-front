import { Component, OnInit } from '@angular/core';
import { PoNotificationService, 
         PoPageAction, 
         PoPageFilter, 
         PoTableAction, 
         PoTableColumn } from '@po-ui/ng-components';
import { Usuario } from '../usuario.model';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista.component.html'
})
export class ListaUsuarioComponent implements OnInit  {

  private readonly url: String = 'http://localhost:4445/usuarios';

  private parametros: {pagina: Number, 
                       pesquisa: String} = {
    pagina: 0,
    pesquisa: ''
  };

  readonly actions: Array<PoPageAction> = [
    { action: this.criarUsuario.bind(this), label: 'Criar Usuário' }
  ];

  public readonly filtro: PoPageFilter = {
    action: this.buscaFiltroPaginado.bind(this),
    placeholder: 'Filtro'
  }

  readonly columns: Array<PoTableColumn> = [
    { property: 'codigo', width: '30%', label: 'Código' },
    { property: 'nome', width: '70%', label: 'Nome' } 
  ];

  public readonly tableActions: Array<PoTableAction> = [
    { action: this.editarUsuario.bind(this), label: 'Editar' },
    { action: this.deletarUsuario.bind(this), label: 'Remover', type: 'danger', separator: true }
  ];

  public filtroStatus: Number = 0;
  public usuarios: Array<Usuario> = [];
  public hasNext: boolean = false;
  public carregando: boolean = false;

  constructor(private usuarioService: UsuarioService, 
              private rotas: Router,
              private poNotification: PoNotificationService) { }

  ngOnInit() {
    this.buscaTudoPaginado();
  }

  private criarUsuario() {
    this.rotas.navigateByUrl('/usuario/novo');
  }

  private editarUsuario(usuario: Usuario) {
    this.rotas.navigateByUrl(`/usuario/editar/${usuario.codigo}`);
  }

  private deletarUsuario(usuario: Usuario) {
    if (usuario.codigo) {
      this.carregando = true;

      this.usuarioService.deletar(usuario.codigo).subscribe(() => {
        this.buscaTudoPaginado();
        this.carregando = false;
        this.mensagem('Usuário deletado com sucesso!');                                                                     
      }) 
    } 
  }

  private buscaFiltroPaginado(filtro: String) {
    this.parametros.pagina = 0;
    this.parametros.pesquisa = filtro; 
    this.buscaTudoPaginado();
  }  

  private buscaTudoPaginado() {
    this.carregando = true;

    this.usuarioService.buscaTudoPaginado(this.parametros).subscribe((resposta) => {
      console.log(resposta);
      this.usuarios = !this.parametros.pagina || this.parametros.pagina === 1 ? resposta.items
                                                                              : [...this.usuarios, ...resposta.items];
      this.carregando = false;                                                                       
    }) 
  } 

  private mensagem (msg: string) {
    this.poNotification.success(msg);
  }

}
